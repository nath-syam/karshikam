<?php
/*
Template Name: Archive Karshikam Products
*/

get_header('buddyheader'); ?>




<div class="mainContainer">
    <div class="container">
        <div class="row">
            <div class="midMainContent col-md-9">
                 <section id="primary" class="content-area">
					<div id="main" class="site-main" role="main">

                       <table class="table table-product-archives">
							 	<tr>
							 		<th>Name</th>
							 		<th>Description</th>
							 		<th>Quantity</th>
							 		<th >Price in <i class="fa fa-inr"></i></th>
							 		<th >Phone number</th>
                  <th></th>
							 	</tr>
					<?php

					if ( have_posts() ) : ?>


						<?php


						// Start the Loop.
						while ( have_posts() ) : the_post();


							 // * Include the Post-Format-specific template for the content.
							 // * If you want to override this in a child theme, then include a file
							 // * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 ?>


							 	<tr>
							 		<td><a title="<?php echo the_title(); ?>" alt="<?php echo the_title(); ?>" href="<?php the_permalink(); ?>"><b><?php echo the_title(); ?></b></a></td>
							 		<td class="kp_descr"><?php echo post_custom('kp_description'); ?> </td>
							 		<td><?php echo post_custom('kp_quantity');?></td>
							 		<td><?php echo post_custom('kp_price');?> <i class="fa fa-inr"></i></td>
							 		<td><?php echo post_custom('kp_phone_number');?></td>
                  <td><a href="<?php the_permalink(); ?>" title="Read more" alt="Read more"><i class="fa fa-arrow-circle-right"></i></a></td>
							 	</tr>



<?php


						// End the loop.
						endwhile;
						?>
							 </table>
<?php endif; ?>
        </div><!-- .site-main -->
				</section><!-- .content-area -->


            </div>
            <div class="rightSidebar col-md-3">
              <div class="taxo-filter">
                  <?php echo do_shortcode( '[acps id="1280"]' ); ?>

              </div>
            <?php get_sidebar('right'); ?>
            </div>
        </div>
</div>
</div>


<?php get_footer(); ?>
