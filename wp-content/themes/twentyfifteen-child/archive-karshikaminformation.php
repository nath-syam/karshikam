<?php
/**
 * Template Name:  Karshikam information archive
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */


get_header('buddyheader'); ?>

<div class="mainContainer">
    <div class="container">
        <div class="row">

          <div class="rightSidebar col-md-3">
            <div class="dtree">
            <?php
            if(function_exists('wpdt_list_categories')){
                wpdt_list_categories('order=ASC');
            }
            ?>
            </div>
            <div class="clearfix"></div>

          <?php get_sidebar('left'); ?>
          </div>
            <div class="midMainContent col-md-6">
                 <section id="primary" class="content-area">
					<main id="main" class="site-main" role="main">

					<?php

					if ( have_posts() ) : ?>


						<?php

						// Start the Loop.
						while ( have_posts() ) : the_post();


							 // * Include the Post-Format-specific template for the content.
							 // * If you want to override this in a child theme, then include a file
							 // * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 ?>
							 <h2><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h2>

							 <p>
							   <?php echo get_the_excerpt(); ?>
							 </p>




<div class="tags">
  <span>Related categories:</span>
   <?php
    $taxonomy = 'karshikaminfocategory';

// get the term IDs assigned to post.
$post_terms = wp_get_object_terms( $post->ID, $taxonomy, array( 'fields' => 'ids' ) );
// separator between links
$separator = ', ';

if ( !empty( $post_terms ) && !is_wp_error( $post_terms ) ) {

	$term_ids = implode( ',' , $post_terms );
	$terms = wp_list_categories( 'title_li=&style=none&echo=0&taxonomy=' . $taxonomy . '&include=' . $term_ids );
	$terms = rtrim( trim( str_replace( '<br />',  $separator, $terms ) ), $separator );

	// display post categories
	echo  $terms;
}
?>
</div>







<?php

						// End the loop.
						endwhile;
						?>

<?php




					endif;
					?>

					</main><!-- .site-main -->
				</section><!-- .content-area -->


            </div>
            <div class="rightSidebar col-md-3">

              <div class="clearfix"></div>
              <div class="taxo-filter">
                  <?php echo do_shortcode( '[acps id="1291"]' ); ?>

              </div>

            <?php get_sidebar('right'); ?>
            </div>
        </dv>
    </div>
</div>


<?php get_footer(); ?>
