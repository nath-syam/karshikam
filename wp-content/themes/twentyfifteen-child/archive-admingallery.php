<?php
/*
Template Name: Archive Karshikam Admin Gallery
*/

get_header('buddyheader'); ?>

<div class="mainContainer">
    <div class="container">
        <dv class="row">
            <div class="midMainContent col-md-9">
                 <section id="primary" class="content-area">
					<main id="main" class="site-main" role="main">

					<?php

					if ( have_posts() ) : ?>


						<?php

						// Start the Loop.
						while ( have_posts() ) : the_post();


							 // * Include the Post-Format-specific template for the content.
							 // * If you want to override this in a child theme, then include a file
							 // * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 ?>
							 <h2><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h2>

							 <?php echo the_content(); ?>




<div class="show-asso-category">
  <span><i class="fa fa-file-text"></i></span>
   <?php
    $taxonomy = 'admingallerycategory';

// get the term IDs assigned to post.
$post_terms = wp_get_object_terms( $post->ID, $taxonomy, array( 'fields' => 'ids' ) );
// separator between links
$separator = ', ';

if ( !empty( $post_terms ) && !is_wp_error( $post_terms ) ) {

	$term_ids = implode( ',' , $post_terms );
	$terms = wp_list_categories( 'title_li=&style=none&echo=0&taxonomy=' . $taxonomy . '&include=' . $term_ids );
	$terms = rtrim( trim( str_replace( '<br />',  $separator, $terms ) ), $separator );

	// display post categories
	echo  $terms;
}
?>
</div>
<div class="show-asso-category">
  <span><i class="fa fa-tags"></i></span>
   <?php
    $taxonomy = 'admingallerytag';

// get the term IDs assigned to post.
$post_terms = wp_get_object_terms( $post->ID, $taxonomy, array( 'fields' => 'ids' ) );
// separator between links
$separator = ', ';

if ( !empty( $post_terms ) && !is_wp_error( $post_terms ) ) {

	$term_ids = implode( ',' , $post_terms );
	$terms = wp_list_categories( 'title_li=&style=none&echo=0&taxonomy=' . $taxonomy . '&include=' . $term_ids );
	$terms = rtrim( trim( str_replace( '<br />',  $separator, $terms ) ), $separator );

	// display post categories
	echo  $terms;
}
?>
</div>







<?php

						// End the loop.
						endwhile;
						?>

<?php




					endif;
					?>

					</main><!-- .site-main -->
				</section><!-- .content-area -->


            </div>
            <div class="rightSidebar col-md-3">
              <div class="taxo-filter">
                  <?php echo do_shortcode( '[acps id="1304"]' ); ?>

              </div>
            <?php get_sidebar('right'); ?>
            </div>
        </dv>
    </div>
</div>


<?php get_footer(); ?>
