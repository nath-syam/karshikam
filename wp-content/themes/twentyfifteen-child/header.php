<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->

	<script>(function(){document.documentElement.className='js'})();</script>
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<div class="top-header container">
		<div class="row">
			<div class="col-md-3">
				<nav class="topSliceMenuleft">
				<ul>
					<li><a href="javascript:void()"><i class="fa fa-calendar-o"></i>  <?php echo date("Y/m/d"); ?></a></li>
					<li><a href="javascript:void()"><i class="fa fa-clock-o"></i>  <?php date_default_timezone_set('Asia/Kolkata'); echo date("h:i:sa"); ?></a></li>
				</ul>
				</nav>
			</div>

			<div class="col-md-9">
				<nav class="topSliceMenu pull-right">
				    <ul>
                      <?php
                        if ( is_user_logged_in() ) {

                        //if logged in do this
                            ?>
                        <li class="noti"><a href="<?php echo esc_url( home_url( '/' ) ); ?>members/<?php global $current_user; get_currentuserinfo(); echo($current_user->user_login); ?>/notifications/"><i class="fa fa-bullhorn"></i> നോട്ടിഫികേഷൻ</a></li>
												<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>about-us"><i class="fa fa-diamond"></i> കാര്ഷികം.ക </a></li>
                        <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>tips-page/"><i class="fa fa-info-circle"></i> ടിപ്സ്</a></li>
												<li><span> സ്വാഗതം  <?php global $current_user; get_currentuserinfo(); echo($current_user->user_login); ?></span><a href="<?php echo wp_logout_url( home_url() ); ?>"><i class="fa fa-sign-out"></i> ലോഗ് ഔട്ട്‌ </a></li>
														<?php } else {
                            //if not logged in do this
                        ?>
													<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>register/"><i class="fa fa-sign-in"></i> രജിസ്റ്റർ </a></li>
                            <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>about-us/"><i class="fa fa-diamond"></i> കാര്ഷികം.ക</a></li>
                        <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>tips-page/"><i class="fa fa-info-circle"></i> ടിപ്സ് ></li>
                                          <?php } ?>
                        </ul>
				</nav>
			</div>
		</div>
</div>
<div class="banner container">
	<div class="nav-previous alignleft">
			<?php echo do_shortcode('[metaslider id=830]'); // malayalam slider  ?>
	</div>
        <a class="header-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="img-responsive header logo" src="<?php  echo get_stylesheet_directory_uri(); ?>/images/brandlogo.png" alt="Karshikam"></a>
</div>

<div class="mainMenu container">
         <div class="row">
             <div class="col-md-12">
               <nav class="mainMenu">
                   <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
               </nav>
             </div>
             <div class="col-md-10"></div>
         </div>
</div>
