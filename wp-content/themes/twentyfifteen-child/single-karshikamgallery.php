<?php
/*
Template Name: Archive Karshikam gallery
*/

get_header('buddyheader'); ?>

<div class="mainContainer">
    <div class="container">
        <dv class="row">
            <div class="midMainContent col-md-9">
                 <section id="primary" class="content-area">
					<div id="main" class="site-main" role="main">

					<?php

					if ( have_posts() ) : ?>


						<?php

						// Start the Loop.
						while ( have_posts() ) : the_post();


							 // * Include the Post-Format-specific template for the content.
							 // * If you want to override this in a child theme, then include a file
							 // * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 ?>
							 <h2><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h2>

               <div class="row archivegallery grid js-masonry" data-masonry-options='{ "itemSelector": ".grid-item"}'>
                              <?php
                   $images = get_post_meta( $post->ID, 'kg_gallery_image' );
                    ?>



                   <?php
                     foreach ( $images as $image ) {
                        ?>
                        <div class="col-md-6 mar-bottom grid-item">


                      <a href="<?php echo  pods_image_url($image, 'original'); ?>" rel="lightbox">  <?php echo pods_image( $image, 'original' );?></a>




                        </div>

                        <?php

                            }
                        ?>

                      </div>
                      <p>
                        <?php echo post_custom('gallery-description');?>
                      </p>
                      <div class="clearfix"></div>
                      <?php echo the_content(); ?>
                      
               <div class="clearfix"></div>




<div class="show-asso-category">
  <span><i class="fa fa-file-text"></i></span>
   <?php
    $taxonomy = 'karshikamgallerycategory';

// get the term IDs assigned to post.
$post_terms = wp_get_object_terms( $post->ID, $taxonomy, array( 'fields' => 'ids' ) );
// separator between links
$separator = ', ';

if ( !empty( $post_terms ) && !is_wp_error( $post_terms ) ) {

	$term_ids = implode( ',' , $post_terms );
	$terms = wp_list_categories( 'title_li=&style=none&echo=0&taxonomy=' . $taxonomy . '&include=' . $term_ids );
	$terms = rtrim( trim( str_replace( '<br />',  $separator, $terms ) ), $separator );

	// display post categories
	echo  $terms;
}
?>
</div>
<div class="show-asso-category">
  <span><i class="fa fa-tags"></i></span>
   <?php
    $taxonomy = 'karshikamgallerytag';

// get the term IDs assigned to post.
$post_terms = wp_get_object_terms( $post->ID, $taxonomy, array( 'fields' => 'ids' ) );
// separator between links
$separator = ', ';

if ( !empty( $post_terms ) && !is_wp_error( $post_terms ) ) {

	$term_ids = implode( ',' , $post_terms );
	$terms = wp_list_categories( 'title_li=&style=none&echo=0&taxonomy=' . $taxonomy . '&include=' . $term_ids );
	$terms = rtrim( trim( str_replace( '<br />',  $separator, $terms ) ), $separator );

	// display post categories
	echo  $terms;
}
?>
</div>







<?php

						// End the loop.
						endwhile;
						?>
            <!-- get the current users -->
            <?php $user_ID = get_current_user_id(); ?>
            <!-- get the author users -->
            <?php $author_ID = get_the_author_ID();  ?>
            <!-- get the postid -->
            <?php  $postid = get_the_ID();  ?>

            <!-- Check wheather the logged in is author -->
            <?php  if ($user_ID  == $author_ID ) {?>
              <div class="alert alert-info" role="alert">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>edit-karshikam-gallery/?id=<?php echo $postid ?>" >Edit this <?php the_title(); ?></a>
              </div>

              <?php } else { ?>
              <div class="alert alert-danger">
                Only the author can edit this product !!
              </div>
              <?php } ?>

<?php




					endif;
					?>

        </div><!-- .site-main -->
				</section><!-- .content-area -->


            </div>
            <div class="rightSidebar col-md-3">
              <div class="taxo-filter">
                  <?php echo do_shortcode( '[acps id="1302"]' ); ?>

              </div>
            <?php get_sidebar('right'); ?>
            </div>
        </dv>
    </div>
</div>


<?php get_footer(); ?>
