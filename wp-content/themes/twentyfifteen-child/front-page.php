<?php
get_header(); ?>

<div class="mainContainer">
    <div class="container">
        <div class="row">
            <div class="leftSidebar col-md-3">
               <?php get_sidebar('left'); ?>
            </div>
            <div class="midMainContent col-md-6">
            <div class="row">
                <div class="col-md-12 home mainContent">
                  <?php
                  // The Query
                  query_posts("post_type=quote&order=rand&posts_per_page=4");
                  ?>
                     <div class="row quotesliderhold">
                       <div class="col-md-12 quoteSlider">
                         <?php

                  // The Loop
                  while ( have_posts() ) : the_post();

                  ?>

                    <div><?php the_post_thumbnail(); ?><a href="<?php echo get_permalink(); ?>">  <p><?php echo home_page_custom_excerpt(); ?></p> </a></div>


                  <?php
                  endwhile;

                  // Reset Query
                  wp_reset_query();
                  ?>
                </div>
                  </div>
                    <div class="row">
                     <?php
                    // The Query
                    query_posts("order=rand&posts_per_page=6");

                    // The Loop
                    while ( have_posts() ) : the_post();

                    ?>
                     <div class="col-md-6 grid-item">
                      <div class="homeMaincontent">
                          <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

                         <p><?php echo home_page_custom_excerpt(); ?></p>
                         <a class="ui primary button readmoreBig" href="<?php echo get_permalink(); ?>">കുടുത്തൽ വായിക്കു</a>
                         </div>
                     </div>

                    <?php
                    endwhile;

                    // Reset Query
                    wp_reset_query();
                    ?>
                    <div class="clearfix"></div>
                </div>
                <div class="row">
                  <div class="col-md-12">  <a href="<?php echo esc_url( home_url( '/' ) ); ?>category/quote-home/" class="allcategoryfull">Read More</a></div>
                </div>

                </div>
            </div>
            </div>
            <div class="rightSidebar col-md-3">
                <?php get_sidebar('right'); ?>
            </div>
        </div>
    </div>
</div>

<?php
get_footer(); ?>
