<?php
/*
Template Name: single Karshikam Products
*/

get_header('buddyheader'); ?>




<div class="mainContainer">
    <div class="container">
        <dv class="row">
            <div class="midMainContent col-md-9">
                 <section id="primary" class="content-area">
					<main id="main" class="site-main" role="main">
            <table class="table table-product-archives">
							 	<tr>
							 		<th>Names</th>
							 		<th>Details</th>
							 	</tr>
					<?php

					if ( have_posts() ) : ?>


						<?php

						// Start the Loop.
						while ( have_posts() ) : the_post();


							 // * Include the Post-Format-specific template for the content.
							 // * If you want to override this in a child theme, then include a file
							 // * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 ?>


							 	<tr>

							 		<td>Name</td>
                  <td>
                    <?php the_title(); ?>
                  </td>
                </tr>
                <tr>
                  <td>Description</td>
                  <td>  <?php echo post_custom('kp_description'); ?></td>
                </tr>
                <tr>
                  <td>Quantity</td>
                  <td>  <?php echo post_custom('kp_quantity'); ?></td>
                </tr>
                <tr>
                  <td>Price</td>
                  <td>  <?php echo post_custom('kp_price'); ?> <i class="fa fa-inr"></i></td>
                </tr>
                <tr>
                  <td>Photo</td>
                  <td>
                    <?php $images = get_post_meta( $post->ID, 'kp_image' ); ?>
                    <?php foreach ( $images as $image ) { ?>
                    <?php echo pods_image( $image, 'thumbnail' ); ?>
                    <?php } ?>
                  </td>
                </tr>
                <tr>
                  <td>Phone Number</td>
                  <td>  <?php echo post_custom('kp_phone_number'); ?></td>
                </tr>
                <tr>
                  <td class="tag-custom">category</td>
                  <td class="tag-custom"><?php echo get_the_term_list( $post->ID, 'karshikamproductcategory'); ?></td>
                </tr>
                <tr>
                  <td class="">Tagged in</td>
                  <td class="tag-custom"><?php echo get_the_term_list( $post->ID, 'karshikamproducttag'); ?></td>
                </tr>
                <tr>
                  <td>Location</td>
                  <td class="tag-custom"><?php echo get_the_term_list( $post->ID, 'karshikamproductlocation'); ?></td>
                </tr>
                <?php

                // End the loop.
                endwhile;
                ?>




							 </table>

            <!-- get the current users -->
            <?php $user_ID = get_current_user_id(); ?>
            <!-- get the author users -->
            <?php $author_ID = get_the_author_ID();  ?>
            <!-- get the postid -->
            <?php  $postid = get_the_ID();  ?>

            <!-- Check wheather the logged in is author -->
            <?php  if ($user_ID  == $author_ID ) {?>
              <div class="alert alert-info" role="alert">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>edit-product/?id=<?php echo $postid ?>" >Edit this <?php the_title(); ?></a>
              </div>

              <?php } else { ?>
              <div class="alert alert-danger">
                Only the author can edit this product !!
              </div>
              <?php } ?>
             <?php ?>
             <?php  endif; ?>
					</main><!-- .site-main -->
				</section><!-- .content-area -->


            </div>
            <div class="rightSidebar col-md-3">
              <div class="taxo-filter">
                  <?php echo do_shortcode( '[acps id="1280"]' ); ?>

              </div>
            <?php get_sidebar('right'); ?>
            </div>
        </dv>
    </div>
</div>


<?php get_footer(); ?>
