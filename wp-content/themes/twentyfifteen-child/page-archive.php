<?php
/**
 * Template Name:  Karshikam information archive
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

 get_header('buddyheader'); ?>

 <div class="mainContainer">
     <div class="container">
         <div class="row">

           <div class="rightSidebar col-md-3">
						 <div class="dtree">
							 <h2>Information Category</h2>
             <?php
             if(function_exists('wpdt_list_categories')){
                 wpdt_list_categories('order=ASC');
             }
             ?>
             </div>
             <div class="clearfix"></div>

           <?php get_sidebar('left'); ?>
           </div>
             <div class="midMainContent col-md-6">
                  <section id="primary" class="content-area">
 					<main id="main" class="site-main" role="main">

						<?php
						// The Query
						query_posts( $args );

						// The Loop
						while ( have_posts() ) : the_post();
						  ?>
							<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
							<p><?php the_excerpt(); ?></p>
							<div class="clearfix"></div>



							<?php
						endwhile;

						// Reset Query
						wp_reset_query();
						?>

 					</main><!-- .site-main -->
 				</section><!-- .content-area -->


             </div>
             <div class="rightSidebar col-md-3">

               <div class="clearfix"></div>
               <div class="taxo-filter">
                   <?php echo do_shortcode( '[acps id="1291"]' ); ?>

               </div>

             <?php get_sidebar('right'); ?>
             </div>
         </dv>
     </div>
 </div>


 <?php get_footer(); ?>
