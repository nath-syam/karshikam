<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
get_header('buddyheader'); ?>

<div class="mainContainer">
    <div class="container">
        <dv class="row">
            <div class="midMainContent col-md-9">
             <div class="row">
                <div class="col-md-12 mainContent">
                    <div id="primary" class="content-area">
                        <main id="main" class="karshik-main" role="main">

                        <?php
                        // Start the loop.
                        while ( have_posts() ) : the_post();

                            // Include the page content template.
                            //get_template_part( 'content', 'page' );

                            // If comments are open or we have at least one comment, load up the comment template.
                            if ( comments_open() || get_comments_number() ) :
                                comments_template();
                            endif;

                        // End the loop.
                        endwhile;
                        ?>

                        </main><!-- .site-main -->
                    </div><!-- .content-area -->
                </div>
            </div>
            </div>
            <div class="rightSidebar col-md-3">
              <?php get_sidebar('right'); ?>
            </div>
        </dv>
    </div>
</div>
<?php
get_footer(); ?>
