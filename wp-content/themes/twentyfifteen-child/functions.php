<?php
/**
 * Twenty Fifteen functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}



/**
 * Enqueue scripts and styles for the front end.
 *
 * @since Twenty Fourteen 1.0
 *
 * @return void
 */
function wpb_adding_scripts() {




 /*Bootstrap css */
wp_register_style('bootstrap-css',  get_stylesheet_directory_uri() . '/bootstrap/dist/css/bootstrap.min.css','','3.0.3', false);
wp_enqueue_style('bootstrap-css');

 /*slick css awsome*/
wp_register_style('slick-css',  get_stylesheet_directory_uri() . '/bootstrap/dist/css/slick.css','','1.5.7', false);
wp_enqueue_style('slick-css');

 /*Font awsome css */
wp_register_style('font-awsome-css',  get_stylesheet_directory_uri() . '/bootstrap/dist/css/font-awesome.min.css','','4.2.0', false);
wp_enqueue_style('font-awsome-css');

/*J moderniser*/
wp_register_script('moderniser',  get_stylesheet_directory_uri() . '/bootstrap/dist/js/modernizr.custom.73986.js','','1.10.2', false);
wp_enqueue_script('moderniser');

/*Semantic ui*/
wp_register_script('semantiic ui',  get_stylesheet_directory_uri() . '/bootstrap/dist/vender/Semantic-UI-CSS-master/semantic.min.js','','1.10.2', false);
wp_enqueue_script('semantiic ui');

 /*shorten-script js*/
wp_register_script('shorten-script',  get_stylesheet_directory_uri() . '/bootstrap/dist/js/jquery.shorten.min.js','','1.0', true);
wp_enqueue_script('shorten-script');

/*custom-slick js*/
wp_register_script('slick-js',  get_stylesheet_directory_uri() . '/bootstrap/dist/js/slick.min.js','','1.0', true);
wp_enqueue_script('slick-js');


wp_register_script('custom-script',  get_stylesheet_directory_uri() . '/bootstrap/dist/js/script.js','','1.0', true);
wp_enqueue_script('custom-script');

}
add_action( 'wp_enqueue_scripts', 'wpb_adding_scripts' );

// Register image size very small
add_image_size( 'verySmall', 50, 50 );



// remove admin bar for all users except admin

add_filter('show_admin_bar', '__return_false');


// regiter menu areas
function register_menu() {
	register_nav_menu('Main-menu', __('Main Menu'));
}
add_action('init', 'register_menu');


// regiter widjet areas

function theme_slug_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Header search home', 'theme-slug' ),
        'id' => 'sidebar-2',
        'description' => __( 'Widgets in this area will be shown on header of home page.', 'theme-slug' ),
    ) );
}
add_action( 'widgets_init', 'theme_slug_widgets_init' );


// unregister menu
add_action( 'after_setup_theme', 'remove_default_menu', 11 );
function remove_default_menu(){
unregister_nav_menu('primary');
unregister_nav_menu('social');
}


/**
 * Modify the footer credits for JetPack Inifite Scroll
 **/
add_filter('infinite_scroll_credit','lc_infinite_scroll_credit');

function lc_infinite_scroll_credit(){
 $content = 'Thanks for the support';
 return $content;
}
/** End JetPack **/


/**
 * Disable contact form 7 styles
 **/

add_filter( 'wpcf7_load_css', '__return_false' );


/**
 * theme support JetPack Inifite Scroll
 **/

add_theme_support( 'infinite-scroll', array(
    'container'  => 'content',
    'footer' => false,
    'render' => 'render_function',
    'wrapper' => false
) );

function render_function() {
    get_template_part('loop');
}




// disable budypress default style
// function my_dequeue_bp_styles() {
//   wp_dequeue_style( 'bp-legacy-css' );
// }
// add_action( 'wp_enqueue_scripts', 'my_dequeue_bp_styles', 20 );





if( !function_exists('remove_display_username_in_single_profile') ):
///////////////////////////////////////////////////////////////////////////////
// Remove @username in single Profile home
//////////////////////////////////////////////////////////////////////////////
function remove_display_username_in_single_profile() { ?>
<style> #item-header-content span.user-nicename { display:none; } </style>
<?php }
add_action('wp_head','remove_display_username_in_single_profile');
endif;



function category_archive_uncategorized_sort( $query ) {
    if ( is_admin() || ! $query->is_main_query() )
        return;

    if ( is_category('uncategorized') ) {
    $query->set( 'orderby', 'title' );
    $query->set( 'order', 'ASC' );
        return;
    }
}
add_action( 'pre_get_posts', 'category_archive_uncategorized_sort', 1 );


//create custom widgets
if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'name'=> 'Language box',
		'id' => 'lang_box',
	));
	register_sidebar(array(
		'name'=> 'Left Sidebar',
		'id' => 'left_sidebar',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'name'=> 'Right Sidebar',
		'id' => 'right_sidebar',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
}


//remove parent style

function child_overwrite_styles() {
    wp_deregister_style( 'fancybox-parent-css' );
}



// hidee admin multisite from subscriber
function removesitemanage_remove_menu_items() {
    if( !current_user_can( 'subscriber' ) ):

	add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );

	function remove_wp_logo( $wp_admin_bar ) {
		$wp_admin_bar->remove_node( 'my-sites' );
	}
    endif;
}
add_action( 'admin_menu', 'removesitemanage_remove_menu_items' );

// hidee admin multisite from subscriber
function removesitemanage_remove_menu_items_newcontent() {
    if( !current_user_can( 'subscriber' ) ):

	add_action( 'admin_bar_menu', 'remove_wp_newcontent', 999 );

	function remove_wp_newcontent( $wp_admin_bar ) {
		$wp_admin_bar->remove_node( 'new-content' );
	}
    endif;
}
add_action( 'admin_menu', 'removesitemanage_remove_menu_items_newcontent' );



// Show only post created by user
function posts_for_current_author($query) {
	global $user_level;

	if($query->is_admin && $user_level < 5) {
		global $user_ID;
		$query->set('author',  $user_ID);
		unset($user_ID);
	}
	unset($user_level);

	return $query;
}
add_filter('pre_get_posts', 'posts_for_current_author');

// disable reistration wp-admuin page
add_action( 'login_head', 'hide_login_nav' );

function hide_login_nav()
{
    ?><style>#nav{display:none}</style><?php
}

//Add rel=lightbox
add_filter('the_content', 'addlightboxrel', 12);
add_filter('get_comment_text', 'addlightboxrel');
function addlightboxrel ($content)
{   global $post;
	$pattern = "/<a(.*?)href=('|\")([^>]*).(bmp|gif|jpeg|jpg|png)('|\")(.*?)>(.*?)<\/a>/i";
    $replacement = '<a$1href=$2$3.$4$5 rel="lightbox['.$post->ID.']"$6>$7</a>';
    $content = preg_replace($pattern, $replacement, $content);
    return $content;
}

// Add custom post type support for wp favorites
// add to functions.php
function custom_field_excerpt($title) {
			global $post;
			$text = get_field($title);
			if ( '' != $text ) {
				$text = strip_shortcodes( $text );
				$text = apply_filters('the_content', $text);
				$text = str_replace(']]>', ']]>', $text);
				$excerpt_length = 20; // 20 words
				$excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
				$text = wp_trim_words( $text, $excerpt_length, $excerpt_more );
			}
			return apply_filters('the_excerpt', $text);
		}


// Add excerpt lenght
function home_page_custom_excerpt(){
  $excerpt = get_the_content();
  $excerpt = strip_shortcodes($excerpt);
  $excerpt = strip_tags($excerpt);
  $the_str = substr($excerpt, 0, 370);
  return $the_str;
}

// Add menus for the footer
register_nav_menus( array(
	'footer_one' => 'Footer menu position one',
	'footer_two' => 'Footer menu position two',
	'footer_three' => 'Footer menu position three',
	'footer_four' => 'Footer menu position four',
) );



// get the body class
add_filter('body_class', function($classes) {
    if (in_array('home', $classes)) {
        wp_enqueue_style('home');
    }
    return $classes;
});


function curPageURL() {
 $pageURL = 'http';
 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 }
 return $pageURL;
}

add_action( 'template_redirect', 'template_redirect_cb' );
function template_redirect_cb() {
    $url = curPageURL();
    if ( strpos( $url,'eab_events_category' ) !== false ) {
        $u = str_replace( "eab_events_category/", "", $url );
        $u = str_replace( "events/", "blog/events/", $u );
        wp_redirect( $u );
    }
}

// rEDIRECT NON LOGGED IN USERS TOLOGIN PAGE
add_action( 'template_redirect', function() {

  if ( is_user_logged_in() || ! is_page() ) return;

  $restricted = array( 679,56 ); // all your restricted pages

  if ( in_array( get_queried_object_id(), $restricted ) ) {
    wp_redirect( site_url( '/user-registration' ) );
    exit();
  }

});


// Add logged in logged out classes in the body

add_filter('body_class','my_class_names');
function my_class_names($classes) {
    if (! ( is_user_logged_in() ) ) {
        $classes[] = 'logged-out';
    } else {
      $classes[] = 'logged-in';
    }
    return $classes;
}

// Add hompage or nothomepage classes in the body

add_filter('body_class','my_class_names_front');
function my_class_names_front($classes) {
    if ( is_home()) {
        $classes[] = 'home-page';
    } else {
      $classes[] = 'not-homepage';
    }
    return $classes;
}
// custom search result page for products
function template_chooser($template)
{
  global $wp_query;
  $post_type = get_query_var('post_type');
  if( $wp_query->is_search && $post_type == 'karshikamproduct' )
  {
    return locate_template('archive-search.php');  //  redirect to archive-search.php
  }
  return $template;
}
add_filter('template_include', 'template_chooser');


add_filter('nav_menu_css_class', 'add_active_class', 10, 2 );


// Add active class to the menus sub links too
add_filter('nav_menu_css_class', 'add_active_class', 10, 2 );

function add_active_class($classes, $item) {

  if( in_array( 'current-menu-item', $classes ) ||
    in_array( 'current-menu-ancestor', $classes ) ||
    in_array( 'current-menu-parent', $classes ) ||
    in_array( 'current_page_parent', $classes ) ||
    in_array( 'current_page_ancestor', $classes )
    ) {

    $classes[] = "active";
  }

  return $classes;
}

function revcon_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Information';
    $submenu['edit.php'][5][0] = 'Information';
    $submenu['edit.php'][10][0] = 'Add Information';
    $submenu['edit.php'][16][0] = 'Information Tags';
    echo '';
}
function revcon_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Informations';
    $labels->singular_name = 'Information';
    $labels->add_new = 'Add Information';
    $labels->add_new_item = 'Add Information';
    $labels->edit_item = 'Edit Information';
    $labels->new_item = 'Information';
    $labels->view_item = 'View Information';
    $labels->search_items = 'Search Information';
    $labels->not_found = 'No Information found';
    $labels->not_found_in_trash = 'No Information found in Trash';
    $labels->all_items = 'All Information';
    $labels->menu_name = 'Information';
    $labels->name_admin_bar = 'Information';
}

add_action( 'admin_menu', 'revcon_change_post_label' );
add_action( 'init', 'revcon_change_post_object' );

// print page template
// add_action('wp_head', 'show_template');
// function show_template() {
//   global $template;
//   print_r($template);
// }







?>
