<?php
/*
Template Name: Archive Karshikam webinar
*/

get_header('buddyheader'); ?>

<div class="mainContainer">
    <div class="container">
        <div class="row">


            <div class="midMainContent col-md-9">
                 <section id="primary" class="content-area">
					<main id="main" class="site-main" role="main">

            <?php
            // Start the loop.
            while ( have_posts() ) : the_post();

              /*
               * Include the post format-specific template for the content. If you want to
               * use this in a child theme, then include a file called called content-___.php
               * (where ___ is the post format) and that will be used instead.
               */
              get_template_part( 'content', get_post_format() );
              ?>
              <div class="show-asso-category">
                <span><i class="fa fa-tags"></i></span>
                 <?php
                  $taxonomy = 'karshikamwebinarcategory';

              // get the term IDs assigned to post.
              $post_terms = wp_get_object_terms( $post->ID, $taxonomy, array( 'fields' => 'ids' ) );
              // separator between links
              $separator = ', ';

              if ( !empty( $post_terms ) && !is_wp_error( $post_terms ) ) {

                $term_ids = implode( ',' , $post_terms );
                $terms = wp_list_categories( 'title_li=&style=none&echo=0&taxonomy=' . $taxonomy . '&include=' . $term_ids );
                $terms = rtrim( trim( str_replace( '<br />',  $separator, $terms ) ), $separator );

                // display post categories
                echo  $terms;
              }
              ?>
              </div>

            <?php

              // If comments are open or we have at least one comment, load up the comment template.
              if ( comments_open() || get_comments_number() ) :
                comments_template();
              endif;

              // Previous/next post navigation.


            // End the loop.
            endwhile;
            ?>

					</main><!-- .site-main -->
				</section><!-- .content-area -->


            </div>
            <div class="rightSidebar col-md-3">

              <div class="clearfix"></div>
              <div class="taxo-filter">
                  <?php echo do_shortcode( '[acps id="1295"]' ); ?>

              </div>

            <?php get_sidebar('right'); ?>
            </div>
        </dv>
    </div>
</div>


<?php get_footer(); ?>
