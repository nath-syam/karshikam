<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

<div class="clearfix"></div>
<footer class="low-footer container">

<div id="footer-fnav">
	<div class="row text-left">
		<div class="col-md-3">
			<h3 class="">Footer Menu First</h3>
			<?php  wp_nav_menu( array( 'theme_location' => 'footer_one' ) ); ?>

		</div><!-- End col-md-4 -->
		<div class="col-md-3">
			<h3 class="">Footer Menu Second</h3>
			<?php  wp_nav_menu( array( 'theme_location' => 'footer_two' ) ); ?>
		</div><!-- End col-md-4 -->
		<div class="col-md-3">
			<h3 class="">Footer Menu Third</h3>
			<?php  wp_nav_menu( array( 'theme_location' => 'footer_three' ) ); ?>

		</div><!-- End col-md-4 -->
		<div class="col-md-3">
<h3 class="">Footer Menu Fourth</h3>
<?php  wp_nav_menu( array( 'theme_location' => 'footer_four' ) ); ?>
		</div><!-- End col-md-4 -->
	</div><!-- End row -->
</div>
<div class="clearfix"></div>

<div class="subscribeFooter">
        <div class="row">
            <div class="col-md-8">
                 <h3>Replace footer about heading</h3>
                      <p class="quote">Replace footer about description</p>
            </div>
            <div class="col-md-4"></div>
        </div>
</div>
</footer>
	<?php wp_footer(); ?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/masonry/3.3.2/masonry.pkgd.min.js"></script>


</body>
</html>
