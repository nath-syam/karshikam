<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header('buddyheader'); ?>




<div class="mainContainer">
    <div class="container">
        <dv class="row">
          <div class="rightSidebar col-md-3">
            <div class="dtree">
              <h2>Information Category</h2>
            <?php
            if(function_exists('wpdt_list_categories')){
                wpdt_list_categories('order=ASC');
            }
            ?>
          </div>
          <?php get_sidebar('left'); ?>
          </div>
            <div class="midMainContent col-md-6">
                 <section id="primary" class="content-area">
					<main id="main" class="site-main" role="main">
            <?php
            // Start the loop.
            while ( have_posts() ) : the_post();

              /*
               * Include the post format-specific template for the content. If you want to
               * use this in a child theme, then include a file called called content-___.php
               * (where ___ is the post format) and that will be used instead.
               */
              get_template_part( 'content', get_post_format() );
              ?>

            <?php

              // If comments are open or we have at least one comment, load up the comment template.
              if ( comments_open() || get_comments_number() ) :
                comments_template();
              endif;

              // Previous/next post navigation.


            // End the loop.
            endwhile;
            ?>


					</main><!-- .site-main -->
				</section><!-- .content-area -->


            </div>
            <div class="rightSidebar col-md-3">

              <div class="clearfix"></div>
              <div class="taxo-filter">
                  <?php echo do_shortcode( '[acps id="1291"]' ); ?>

              </div>

            <?php get_sidebar('right'); ?>
            </div>
        </dv>
    </div>
</div>
<?php
get_footer(); ?>
