<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>



<div id="content" class="site-content">

<article id="post-<?php the_ID(); ?>" <?php post_class('wow fadeIn'); ?>>
	<?php
		// Post thumbnail.
		twentyfifteen_post_thumbnail();
	?>

	<header class="entry-header">
		<?php
			if ( is_single() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
			endif;
		?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			/* translators: %s: Name of current post */
			the_content( sprintf(
				__( 'Continue reading %s', 'twentyfifteen' ),
				the_title( '<span class="screen-reader-text">', '</span>', false )
			) );




			if ( 'college' == get_post_type() ) {

				/* Custom code for 'movie' post type. */
				?>
				<div class="row college-address-college-single">
					<div class="col-md-4">
						<b>Address</b>
						<p>
							<address> <?php the_field('college_contact_address'); ?></address>
						</p>
					</div>
					<div class="col-md-4">
						<!-- coming soon -->
					</div>
					<div class="col-md-4">
						<!-- coming soon -->
					</div>
				</div>
				<?php 

			} 


			if ( 'college' == get_post_type() ) {

				/* Custom code for 'movie' post type. */
				?>
				<a class="go-to-college-single" href="<?php echo get_permalink(); ?>">More details</a>
				<?php 

			} 


			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentyfifteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );
		?>
	</div><!-- .entry-content -->

	<?php
		// Author bio.
		if ( is_single() && get_the_author_meta( 'description' ) ) :
			get_template_part( 'author-bio' );
		endif;
	?>

	<footer class="entry-footer">
		<?php twentyfifteen_entry_meta(); ?>
		<?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->

</article><!-- #post-## -->

</div><!-- .site-content -->