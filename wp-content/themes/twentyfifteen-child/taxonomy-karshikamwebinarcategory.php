<?php
/*
Template Name: Archive Karshikam webinar
*/

get_header('buddyheader'); ?>

<div class="mainContainer">
    <div class="container">
        <div class="row">
            <div class="midMainContent col-md-9">
                 <section id="primary" class="content-area">
					<main id="main" class="site-main" role="main">

            <?php while ( have_posts() ) : the_post(); ?>

            				<?php get_template_part( 'content', 'page' ); ?>

            				<?php
            					// If comments are open or we have at least one comment, load up the comment template
            					if ( comments_open() || get_comments_number() ) :
            						comments_template();
            					endif;
            				?>

            		<?php endwhile; // end of the loop. ?>

                    <?php wp_reset_postdata(); ?>

            		<?php

                    $args = array( 'post_type' => 'karshikamwebinar', 'posts_per_page' => 10 );
                    $loop = new WP_Query( $args );
                    while ( $loop->have_posts() ) : $loop->the_post(); ?>

                    <a href="<?php the_permalink(); ?>"><h2><?php echo  the_title(); ?></h2></a>
                    <?php the_excerpt(); ?>

                    <div class="show-asso-category">
                      <span><i class="fa fa-tags"></i></span>
                       <?php
                        $taxonomy = 'karshikamwebinarcategory';

                    // get the term IDs assigned to post.
                    $post_terms = wp_get_object_terms( $post->ID, $taxonomy, array( 'fields' => 'ids' ) );
                    // separator between links
                    $separator = ', ';

                    if ( !empty( $post_terms ) && !is_wp_error( $post_terms ) ) {

                    	$term_ids = implode( ',' , $post_terms );
                    	$terms = wp_list_categories( 'title_li=&style=none&echo=0&taxonomy=' . $taxonomy . '&include=' . $term_ids );
                    	$terms = rtrim( trim( str_replace( '<br />',  $separator, $terms ) ), $separator );

                    	// display post categories
                    	echo  $terms;
                    }
                    ?>
                    </div>


            		<?php endwhile; // end of the loop. ?>

                    <?php wp_reset_postdata(); ?>

					</main><!-- .site-main -->
				</section><!-- .content-area -->


            </div>
            <div class="rightSidebar col-md-3">

              <div class="clearfix"></div>
              <div class="taxo-filter">
                  <?php echo do_shortcode( '[acps id="1295"]' ); ?>

              </div>

            <?php get_sidebar('right'); ?>
            </div>
        </dv>
    </div>
</div>


<?php get_footer(); ?>
