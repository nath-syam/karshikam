<?php
//New results loop
$acps_results = new WP_Query($this->acps_args);
$template = get_option('template');
$search = ( isset($this->acps_args['s']) ) ? true : false;
$postType = $_POST['acps_post_type'];
/*
 * Product Search
 */
if ($postType == 'karshikamproduct') {
    get_header('buddyheader');
    ?>
    <div class="mainContainer">
        <div class="container">
            <div class="row">
                <div class="midMainContent col-md-9">
                    <section id="primary" class="content-area">

                        <div id="main" class="site-main" role="main">
                            <?php if ($acps_results->have_posts()): ?>

                                <table class="table table-product-archives">
                                    <tr>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Quantity</th>
                                        <th >Price in <i class="fa fa-inr"></i></th>
                                        <th >Phone number</th>
                                        <th></th>
                                    </tr>


                                    <?php
                                    // Start the Loop.
                                    while ($acps_results->have_posts()) : $acps_results->the_post();


                                        // * Include the Post-Format-specific template for the content.
                                        // * If you want to override this in a child theme, then include a file
                                        // * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                        ?>


                                        <tr>
                                            <td><a title="<?php echo the_title(); ?>" alt="<?php echo the_title(); ?>" href="<?php the_permalink(); ?>"><b><?php echo the_title(); ?></b></a></td>
                                            <td class="kp_descr"><?php echo post_custom('kp_description'); ?> </td>
                                            <td><?php echo post_custom('kp_quantity'); ?></td>
                                            <td><?php echo post_custom('kp_price'); ?> <i class="fa fa-inr"></i></td>
                                            <td><?php echo post_custom('kp_phone_number'); ?></td>
                                            <td><a href="<?php the_permalink(); ?>" title="Read more" alt="Read more"><i class="fa fa-arrow-circle-right"></i></a></td>
                                        </tr>



                                        <?php
                                    // End the loop.
                                    endwhile;
                                    ?>
                                </table>
                                <?php
                            else:
                                echo '<h1 class="page-title">' . _e('Nothing Found', 'acps') . '</h1>';
                            endif;
                            ?>
                        </div><!-- .site-main -->

                    </section><!-- .content-area -->


                </div>
                <div class="rightSidebar col-md-3">
                    <div class="taxo-filter">
                        <?php
//echo do_shortcode('[acps id="1280"]'); 
                        echo do_shortcode('[acps id="' . $this->acps_form_id . '"]');
                        ?>

                    </div>
                    <?php get_sidebar('right'); ?>
                </div>
            </div>
        </div>
    </div>


    <?php get_footer(); ?>

    <?php
}
/*
 * Karshika information
 */
elseif ($postType == 'karshikaminformation') {
    get_header('buddyheader');
    ?>
    <div class="mainContainer">
        <div class="container">
            <div class="row">

                <div class="rightSidebar col-md-3">
                    <div class="dtree">
                        <h2>Information Category</h2>
                        <?php
                        if (function_exists('wpdt_list_categories')) {
                            wpdt_list_categories('order=ASC');
                        }
                        ?>
                    </div>
                    <div class="clearfix"></div>

                    <?php get_sidebar('left'); ?>
                </div>
                <div class="midMainContent col-md-6">
                    <section id="primary" class="content-area">
                        <main id="main" class="site-main" role="main">
                            <?php if ($acps_results->have_posts()): ?>
                                <?php
                                // The Query
                                //query_posts( $args );
                                // The Loop
                                while ($acps_results->have_posts()) : $acps_results->the_post();
                                    ?>
                                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                    <p><?php the_excerpt(); ?></p>
                                    <div class="clearfix"></div>



                                    <?php
                                endwhile;

                                // Reset Query
                                //wp_reset_query();
                                ?>
                                <?php
                            else:
                                echo '<h1 class="page-title">' . _e('Nothing Found', 'acps') . '</h1>';
                            endif;
                            ?>
                        </main><!-- .site-main -->
                    </section><!-- .content-area -->


                </div>
                <div class="rightSidebar col-md-3">

                    <div class="clearfix"></div>
                    <div class="taxo-filter">
                        <?php echo do_shortcode('[acps id="' . $this->acps_form_id . '"]'); ?>

                    </div>

                    <?php get_sidebar('right'); ?>
                </div>
                </dv>
            </div>
        </div>
        <?php
        get_footer();
    }
    elseif ($postType == 'admingallery') {
        get_header('buddyheader');
        ?>
        <div class="mainContainer">
            <div class="container">
                <dv class="row">
                    <div class="midMainContent col-md-9">
                        <section id="primary" class="content-area">
                            <main id="main" class="site-main" role="main">

                                <?php if ($acps_results->have_posts()) : ?>


                                    <?php
                                    // Start the Loop.
                                    while ($acps_results->have_posts()) : $acps_results->the_post();


                                        // * Include the Post-Format-specific template for the content.
                                        // * If you want to override this in a child theme, then include a file
                                        // * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                        ?>
                                        <h2><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h2>

                                        <?php echo the_content(); ?>




                                        <div class="show-asso-category">
                                            <span><i class="fa fa-file-text"></i></span>
                                            <?php
                                            $taxonomy = 'admingallerycategory';

// get the term IDs assigned to post.
                                            $post_terms = wp_get_object_terms($post->ID, $taxonomy, array('fields' => 'ids'));
// separator between links
                                            $separator = ', ';

                                            if (!empty($post_terms) && !is_wp_error($post_terms)) {

                                                $term_ids = implode(',', $post_terms);
                                                $terms = wp_list_categories('title_li=&style=none&echo=0&taxonomy=' . $taxonomy . '&include=' . $term_ids);
                                                $terms = rtrim(trim(str_replace('<br />', $separator, $terms)), $separator);

                                                // display post categories
                                                echo $terms;
                                            }
                                            ?>
                                        </div>
                                        <div class="show-asso-category">
                                            <span><i class="fa fa-tags"></i></span>
                                            <?php
                                            $taxonomy = 'admingallerytag';

// get the term IDs assigned to post.
                                            $post_terms = wp_get_object_terms($post->ID, $taxonomy, array('fields' => 'ids'));
// separator between links
                                            $separator = ', ';

                                            if (!empty($post_terms) && !is_wp_error($post_terms)) {

                                                $term_ids = implode(',', $post_terms);
                                                $terms = wp_list_categories('title_li=&style=none&echo=0&taxonomy=' . $taxonomy . '&include=' . $term_ids);
                                                $terms = rtrim(trim(str_replace('<br />', $separator, $terms)), $separator);

                                                // display post categories
                                                echo $terms;
                                            }
                                            ?>
                                        </div>







                                        <?php
                                    // End the loop.
                                    endwhile;
                                    ?>

                                    <?php
                                else:
                                    echo '<h1 class="page-title">' . _e('Nothing Found', 'acps') . '</h1>';
                                endif;
                                ?>

                            </main><!-- .site-main -->
                        </section><!-- .content-area -->


                    </div>
                    <div class="rightSidebar col-md-3">
                        <div class="taxo-filter">
                            <?php echo do_shortcode('[acps id="' . $this->acps_form_id . '"]'); ?>

                        </div>
                        <?php get_sidebar('right'); ?>
                    </div>
                </dv>
            </div>
        </div>
        <?php
        get_footer();
    }
    elseif ($postType == 'karshikamgallery') {
        get_header('buddyheader');
        ?>
        <div class="mainContainer">
            <div class="container">
                <dv class="row">
                    <div class="midMainContent col-md-9">
                        <section id="primary" class="content-area">
                            <div id="main" class="site-main" role="main">

                                <?php if ($acps_results->have_posts()) : ?>


                                    <?php
                                    // Start the Loop.
                                    while ($acps_results->have_posts()) : $acps_results->the_post();


                                        // * Include the Post-Format-specific template for the content.
                                        // * If you want to override this in a child theme, then include a file
                                        // * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                        ?>
                                        <div class="midMainContent col-md-4">
                                            <h2><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h2>

                                            <div class="row archivegallery grid" data-masonry-options='{ "itemSelector": ".grid-item"}'>
                                                <div class="gallerygridxd">

                                                    <?php
                                                    $images = get_post_meta($post->ID, 'kg_gallery_image');
                                                    ?>



                                                    <?php
                                                    foreach ($images as $image) {
                                                        ?>
                                                        <div class="mar-bottom grid-item">


                                                            <a href="<?php the_permalink(); ?>"> <?php echo pods_image($image, 'thumbnail'); ?></a>

                                                        </div>

                                                        <?php
                                                    }
                                                    ?>

                                                </div>
                                                <div class="clearfix">

                                                </div>

                                                <?php //echo the_content();  ?>




                                                <div class="show-asso-category">
                                                    <span><i class="fa fa-file-text"></i></span>
                                                    <?php
                                                    $taxonomy = 'karshikamgallerycategory';

// get the term IDs assigned to post.
                                                    $post_terms = wp_get_object_terms($post->ID, $taxonomy, array('fields' => 'ids'));
// separator between links
                                                    $separator = ', ';

                                                    if (!empty($post_terms) && !is_wp_error($post_terms)) {

                                                        $term_ids = implode(',', $post_terms);
                                                        $terms = wp_list_categories('title_li=&style=none&echo=0&taxonomy=' . $taxonomy . '&include=' . $term_ids);
                                                        $terms = rtrim(trim(str_replace('<br />', $separator, $terms)), $separator);

                                                        // display post categories
                                                        echo $terms;
                                                    }
                                                    ?>
                                                </div>
                                                <div class="show-asso-category">
                                                    <span><i class="fa fa-tags"></i></span>
                                                    <?php
                                                    $taxonomy = 'karshikamgallerytag';

// get the term IDs assigned to post.
                                                    $post_terms = wp_get_object_terms($post->ID, $taxonomy, array('fields' => 'ids'));
// separator between links
                                                    $separator = ', ';

                                                    if (!empty($post_terms) && !is_wp_error($post_terms)) {

                                                        $term_ids = implode(',', $post_terms);
                                                        $terms = wp_list_categories('title_li=&style=none&echo=0&taxonomy=' . $taxonomy . '&include=' . $term_ids);
                                                        $terms = rtrim(trim(str_replace('<br />', $separator, $terms)), $separator);

                                                        // display post categories
                                                        echo $terms;
                                                    }
                                                    ?>
                                                </div>





                                            </div>

                                        </div>
                                        <?php
                                    // End the loop.
                                    endwhile;
                                    ?>
                                    <?php
                                else:
                                    echo '<h1 class="page-title">' . _e('Nothing Found', 'acps') . '</h1>';
                                endif;
                                ?>

                            </div><!-- .site-main -->
                        </section><!-- .content-area -->


                    </div>

                    <div class="rightSidebar col-md-3">
                        <div class="taxo-filter">
                            <?php echo do_shortcode('[acps id="' . $this->acps_form_id . '"]'); ?>

                        </div>
                        <?php get_sidebar('right'); ?>
                    </div>

                </dv>
            </div>
        </div>
        <?php
        get_footer();
    }
    elseif ($postType == 'karshikamwebinar') {
        get_header('buddyheader');
        ?>
        <div class="mainContainer">
            <div class="container">
                <div class="row">
                    <div class="midMainContent col-md-9">
                        <section id="primary" class="content-area">
                            <main id="main" class="site-main" role="main">

                                <?php while ($acps_results->have_posts()) : $acps_results->the_post(); ?>

                                    <?php get_template_part('content', 'page'); ?>

                                    <?php
                                    // If comments are open or we have at least one comment, load up the comment template
                                    if (comments_open() || get_comments_number()) :
                                        comments_template();
                                    endif;
                                    ?>

                                <?php endwhile; // end of the loop. ?>

                                <?php wp_reset_postdata(); ?>

                                <?php
                                $args = array('post_type' => 'karshikamwebinar', 'posts_per_page' => 10);
                                $loop = new WP_Query($args);
                                while ($loop->have_posts()) : $loop->the_post();
                                    ?>

                                    <a href="<?php the_permalink(); ?>"><h2><?php echo the_title(); ?></h2></a>
                                    <?php the_excerpt(); ?>

                                    <div class="show-asso-category">
                                        <span><i class="fa fa-tags"></i></span>
                                        <?php
                                        $taxonomy = 'karshikamwebinarcategory';

                                        // get the term IDs assigned to post.
                                        $post_terms = wp_get_object_terms($post->ID, $taxonomy, array('fields' => 'ids'));
                                        // separator between links
                                        $separator = ', ';

                                        if (!empty($post_terms) && !is_wp_error($post_terms)) {

                                            $term_ids = implode(',', $post_terms);
                                            $terms = wp_list_categories('title_li=&style=none&echo=0&taxonomy=' . $taxonomy . '&include=' . $term_ids);
                                            $terms = rtrim(trim(str_replace('<br />', $separator, $terms)), $separator);

                                            // display post categories
                                            echo $terms;
                                        }
                                        ?>
                                    </div>


                                <?php endwhile; // end of the loop. ?>

                                <?php wp_reset_postdata(); ?>

                            </main><!-- .site-main -->
                        </section><!-- .content-area -->


                    </div>
                    <div class="rightSidebar col-md-3">

                        <div class="clearfix"></div>
                        <div class="taxo-filter">
                            <?php echo do_shortcode('[acps id="' . $this->acps_form_id . '"]'); ?>

                        </div>

                        <?php get_sidebar('right'); ?>
                    </div>
                    </dv>
                </div>
            </div>
            <?php
            get_footer();
        } elseif ($postType == 'karshikamdiary') {
            get_header('buddyheader');
            ?>
            <div class="mainContainer">
                <div class="container">
                    <dv class="row">
                        <div class="midMainContent col-md-9">
                            <section id="primary" class="content-area">
                                <main id="main" class="site-main" role="main">



                                    <?php if ($acps_results->have_posts()) : ?>
                                        <table class="table table-contacts">
                                            <tr>
                                                <th class="name">Name</th>
                                                <th class="address">Address</th>
                                                <th class="about">About</th>
                                                <th class="phnumber">Phone number</th>
                                                <th class="category">Categories</th>
                                                <th class="tags" >Tags</th>
                                            </tr>

                                            <?php
                                            // Start the Loop.
                                            while ($acps_results->have_posts()) : $acps_results->the_post();


                                                // * Include the Post-Format-specific template for the content.
                                                // * If you want to override this in a child theme, then include a file
                                                // * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                                ?>
                                                <tr>
                                                    <td class="name"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></td>
                                                    <td class="address"><?php echo post_custom('karshikam_diary_address'); ?></td>
                                                    <td class="about"><?php echo post_custom('karshikam_diary_about'); ?></td>
                                                    <td class="phnumber"><?php echo post_custom('karshikam_diary_phone_number'); ?></td>
                                                    <td class="category"><div class="">
                                                            <?php
                                                            $taxonomy = 'karshikamdiaryexpertcategory';

// get the term IDs assigned to post.
                                                            $post_terms = wp_get_object_terms($post->ID, $taxonomy, array('fields' => 'ids'));
// separator between links
                                                            $separator = ', ';

                                                            if (!empty($post_terms) && !is_wp_error($post_terms)) {

                                                                $term_ids = implode(',', $post_terms);
                                                                $terms = wp_list_categories('title_li=&style=none&echo=0&taxonomy=' . $taxonomy . '&include=' . $term_ids);
                                                                $terms = rtrim(trim(str_replace('<br />', $separator, $terms)), $separator);

                                                                // display post categories
                                                                echo $terms;
                                                            }
                                                            ?>
                                                        </div> </td>
                                                    <td class="tags">
                                                        <div class="">
                                                            <?php
                                                            $taxonomy = 'karshikamdiaryexpertcategory';

// get the term IDs assigned to post.
                                                            $post_terms = wp_get_object_terms($post->ID, $taxonomy, array('fields' => 'ids'));
// separator between links
                                                            $separator = ', ';

                                                            if (!empty($post_terms) && !is_wp_error($post_terms)) {

                                                                $term_ids = implode(',', $post_terms);
                                                                $terms = wp_list_categories('title_li=&style=none&echo=0&taxonomy=' . $taxonomy . '&include=' . $term_ids);
                                                                $terms = rtrim(trim(str_replace('<br />', $separator, $terms)), $separator);

                                                                // display post categories
                                                                echo $terms;
                                                            }
                                                            ?>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <?php
                                            // End the loop.
                                            endwhile;
                                            ?>
                                        </table>
                                        <?php
                                    else:
                                        echo '<h1 class="page-title">' . _e('Nothing Found', 'acps') . '</h1>';
                                    endif;
                                    ?>


                                </main><!-- .site-main -->
                            </section><!-- .content-area -->


                        </div>
                        <div class="rightSidebar col-md-3">
                            <div class="taxo-filter">
                                <?php echo do_shortcode('[acps id="' . $this->acps_form_id . '"]'); ?>

                            </div>
                            <?php get_sidebar('right'); ?>
                        </div>
                    </dv>
                </div>
            </div>
            <?php
            get_footer();
        }
        else{
            get_header('buddyheader');
            get_footer();
        }
        ?>