   
	<?php
    while ( have_posts() ) : the_post();
	
		//Content includes shortcode (only remove if you are calling it directly in your template)
		the_content();
		
	endwhile;
	
	//Reset postdata (avoid potential conflicts)
	wp_reset_postdata();
	?>
