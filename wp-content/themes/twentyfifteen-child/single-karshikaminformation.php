<?php
/*

*/

get_header('buddyheader'); ?>

<div class="mainContainer">
    <div class="container">
        <dv class="row">
          <div class="rightSidebar col-md-3">
          <div class="dtree">
          <?php
          if(function_exists('wpdt_list_categories')){
              wpdt_list_categories('post_type=karshikaminformation&order=ASC');
          }
          ?>
          </div>
            <div class="clearfix"></div>

          <?php get_sidebar('left'); ?>
          </div>
            <div class="midMainContent col-md-6">
                 <section id="primary" class="content-area">
					<main id="main" class="site-main" role="main">

					<?php

					if ( have_posts() ) : ?>


						<?php

						// Start the Loop.
						while ( have_posts() ) : the_post();


							 // * Include the Post-Format-specific template for the content.
							 // * If you want to override this in a child theme, then include a file
							 // * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 ?>
							 <h2><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h2>

							 <?php echo the_content(); ?>

<?php

						// End the loop.
						endwhile;
						?>


            <!-- get the current users -->
            <?php $user_ID = get_current_user_id(); ?>
            <!-- get the author users -->
            <?php $author_ID = get_the_author_ID();  ?>
            <!-- get the postid -->
            <?php  $postid = get_the_ID();  ?>

            <!-- Check wheather the logged in is author -->
            <?php  if ($user_ID  == $author_ID ) {?>
              <div class="alert alert-info" role="alert">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>edit-information/?id=<?php echo $postid ?>" >Edit this <?php the_title(); ?></a>
              </div>

              <?php } else { ?>
              <div class="alert alert-danger">
                Only the author can edit this product !!
              </div>
              <?php } ?>
             <?php ?>

<?php




					endif;
					?>

					</main><!-- .site-main -->
				</section><!-- .content-area -->


            </div>
            <div class="rightSidebar col-md-3">
              <div class="taxo-filter">
                  <?php echo do_shortcode( '[acps id="1291"]' ); ?>

              </div>
            <?php get_sidebar('right'); ?>
            </div>
        </dv>
    </div>
</div>


<?php get_footer(); ?>
