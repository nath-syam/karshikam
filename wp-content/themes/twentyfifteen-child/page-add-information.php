<?php
/**
 * Template Name: Add Karshikam information
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header('buddyheader') ?>


<div class="mainContainer">
    <div class="container">
        <dv class="row">
            <div class="leftSidebar col-md-3">




            <?php get_sidebar('left'); ?>
            </div>
            <div class="midMainContent col-md-9">
             <div class="row">
                <div class="col-md-12 mainContent">
                    <div id="primary" class="content-area">
                        <div id="main" class="karshik-main" role="main">
                          <?php
                          $id = $_GET['id'];
                          $mypod = pods( 'karshikaminformation' );?>
                          <h2>Add information</h2>
                          <?php
                          // Only show the 'name', 'description', and 'other' fields.
                          $fields = array(
                            'karshikam_information_language',
                            'post_content',
                          );
                          echo $mypod->form( $fields );
                           ?>

                        </div><!-- .site-main -->
                    </div><!-- .content-area -->
                </div>
            </div>
            </div>
        </dv>
    </div>
</div>
<?php
get_footer(); ?>