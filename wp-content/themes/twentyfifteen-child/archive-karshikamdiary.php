<?php
/*
Template Name: Archive Karshikam Diary
*/

get_header('buddyheader'); ?>

<div class="mainContainer">
    <div class="container">
        <dv class="row">
            <div class="midMainContent col-md-9">
                 <section id="primary" class="content-area">
					<main id="main" class="site-main" role="main">

                        <table class="table table-contacts">
                            <tr>
                                <th class="name">Name</th>
                                <th class="address">Address</th>
                                <th class="about">About</th>
                                <th class="phnumber">Phone number</th>
                                <th class="category">Categories</th>
                                <th class="tags" >Tags</th>
                            </tr>

					<?php


					if ( have_posts() ) : ?>


						<?php

						// Start the Loop.
						while ( have_posts() ) : the_post();


							 // * Include the Post-Format-specific template for the content.
							 // * If you want to override this in a child theme, then include a file
							 // * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 ?>
                            <tr>
                                <td class="name"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></td>
                                <td class="address"><?php echo post_custom('karshikam_diary_address'); ?></td>
                                <td class="about"><?php echo post_custom('karshikam_diary_about'); ?></td>
                                <td class="phnumber"><?php echo post_custom('karshikam_diary_phone_number'); ?></td>
                                <td class="category"><div class="">
   <?php
    $taxonomy = 'karshikamdiaryexpertcategory';

// get the term IDs assigned to post.
$post_terms = wp_get_object_terms( $post->ID, $taxonomy, array( 'fields' => 'ids' ) );
// separator between links
$separator = ', ';

if ( !empty( $post_terms ) && !is_wp_error( $post_terms ) ) {

	$term_ids = implode( ',' , $post_terms );
	$terms = wp_list_categories( 'title_li=&style=none&echo=0&taxonomy=' . $taxonomy . '&include=' . $term_ids );
	$terms = rtrim( trim( str_replace( '<br />',  $separator, $terms ) ), $separator );

	// display post categories
	echo  $terms;
}
?>
</div> </td>
                                <td class="tags">
                                <div class="">
   <?php
    $taxonomy = 'karshikamdiaryexpertcategory';

// get the term IDs assigned to post.
$post_terms = wp_get_object_terms( $post->ID, $taxonomy, array( 'fields' => 'ids' ) );
// separator between links
$separator = ', ';

if ( !empty( $post_terms ) && !is_wp_error( $post_terms ) ) {

	$term_ids = implode( ',' , $post_terms );
	$terms = wp_list_categories( 'title_li=&style=none&echo=0&taxonomy=' . $taxonomy . '&include=' . $term_ids );
	$terms = rtrim( trim( str_replace( '<br />',  $separator, $terms ) ), $separator );

	// display post categories
	echo  $terms;
}
?>
</div>
                                </td>
                            </tr>
















<?php

						// End the loop.
						endwhile;
						?>

<?php




					endif;
					?>
                                                    </table>

					</main><!-- .site-main -->
				</section><!-- .content-area -->


            </div>
            <div class="rightSidebar col-md-3">
              <div class="taxo-filter">
                  <?php echo do_shortcode( '[acps id="1305"]' ); ?>

              </div>
            <?php get_sidebar('right'); ?>
            </div>
        </dv>
    </div>
</div>


<?php get_footer(); ?>
