<?php
/**
 * Template Name: Welcome to karshikam (Dashboard)

 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
get_header('buddyheader'); ?>

<div class="mainContainer welcome_karshikam">
    <div class="container">
        <dv class="row">
            <div class="leftSidebar col-md-3">
            <?php get_sidebar('left'); ?>
            </div>
            <div class="midMainContent col-md-9">
             <div class="row">
                <div class="col-md-12 mainContent">
                    <div id="primary" class="content-area">
                        <main id="main" class="karshik-main" role="main">

                        <?php
                        // Start the loop.
                        while ( have_posts() ) : the_post();

                            // Include the page content template.
                            get_template_part( 'content', 'page' );
                        // End the loop.
                        endwhile;  wp_reset_query();
                        ?>

<!-- karshikam products starts -->

                        <?php

                        // Edit the karshikam products created by current user
                        // get the username
                        $current_user = wp_get_current_user();
                        $username = $current_user->user_login;
                        $user_ID = get_current_user_id();


                            $loop = new WP_Query( array( 'post_type' => 'karshikamproduct', 'ignore_sticky_posts' => 1, 'author' => $user_ID ) );
                            if ( $loop->have_posts() ) :

                              ?>
                              <h3>My Products</h3>
                              <table class="table">
                              <tr>
                                <th style="width: 80%;">Product Name</th>
                                <th style="width: 20%;">Action</th>
                              </tr>
                              <?php
                                while ( $loop->have_posts() ) : $loop->the_post();
                                $postkarshikid = get_the_ID();
                                ?>


                                <tr>
                                  <td style="width: 80%;"><a target="_blank" href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></td>
                                  <td style="width: 20%;"><a target="_blank" href="<?php echo esc_url( home_url( '/' ) ); ?>edit-product/?id=<?php echo $postkarshikid ?>">Edit</a></td>
                               </tr>


                                <?php endwhile;
                                ?>
                              </table>
                              <?php
                              endif;

                          wp_reset_postdata();
                        ?>
                        <!-- karshikam products ends  -->

                        <!-- edit karshikam information -->

                        <?php
                        $loop = new WP_Query( array( 'post_type' => 'karshikaminformation', 'ignore_sticky_posts' => 1, 'author' => $user_ID ) );
                        if ( $loop->have_posts() ) :
                          ?>
                          <h3>My Information</h3>
                          <table class="table">
                          <tr>
                            <th style="width: 80%;">Information name</th>
                            <th style="width: 20%;">Action</th>
                          </tr>
                          <?php
                            while ( $loop->have_posts() ) : $loop->the_post();
                            $postkarshikinfoid = get_the_ID();
                            ?>

                            <tr>
                              <td><a target="_blank" href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></td>
                              <td><a target="_blank" href="<?php echo esc_url( home_url( '/' ) ); ?>edit-information/?id=<?php echo $postkarshikinfoid ?>">Edit</a></td>
                            </tr>

                            <?php endwhile;
                            ?>
                          </table>
                          <?php
                          endif;
                        wp_reset_postdata();
                    ?>
                    <!-- karshikam information ends  -->

                    <!-- edit karshikam gallery -->

                    <?php
                    $loop = new WP_Query( array( 'post_type' => 'karshikamgallery', 'ignore_sticky_posts' => 1, 'author' => $user_ID ) );
                    if ( $loop->have_posts() ) :
                      ?>
                      <h3>My Gallery</h3>
                      <table class="table">
                      <tr>
                        <th style="width: 80%;">Information name</th>
                        <th style="width: 20%;">Action</th>
                      </tr>
                      <?php
                        while ( $loop->have_posts() ) : $loop->the_post();
                        $postkarshikgalleryid = get_the_ID();
                        ?>

                        <tr>
                          <td style="width: 80%;"><a target="_blank" href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></td>
                          <td style="width: 20%;"><a target="_blank" href="<?php echo esc_url( home_url( '/' ) ); ?>edit-karshikam-gallery/?id=<?php echo $postkarshikgalleryid ?>">Add Images</a></td>
                        </tr>



                        <?php endwhile;
                        ?>
                      </table>
                      <?php
                      endif;
                    wp_reset_postdata();
                ?>
                <!-- karshikam information ends  -->

                        </main><!-- .site-main -->
                    </div><!-- .content-area -->
                </div>
            </div>
            </div>
        </dv>
    </div>
</div>
<?php
get_footer(); ?>
